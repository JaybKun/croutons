CC=gcc
CFLAGS=-c -Wall
LDFLAGS=

LIBS=-pthread

INCLUDE=-I/home/jay/kernel/scheduler_kernel/include/linux -I/home/jay/kernel/scheduler_kernel/include/uapi/linux

SOURCES=threadsort.c

TARGET=threadsort

OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) $(LDFLAGS) $(INCLUDE) $(LIBS) $(OBJECTS) -o $@
 
.cpp.o :
	$(CC) $(CFLAGS) $(INCLUDE) $(LIBS) $< -o $@

clean:
	rm -f $(OBJECTS) $(TARGET)
