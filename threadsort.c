#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
//--------------------------------------------------------------
struct sortingArray {
        pthread_t thread_id;
        int size;
        float* array;
};
//--------------------------------------------------------------
void* quicksort(void* sortingArray);
void displaySA(struct sortingArray* array, int bShowValues);
void initSA(struct sortingArray* array, int iSize);
//--------------------------------------------------------------
int main()
{
        int iSize = 1000000;
        int iScaling = 10;
        pthread_t fastThread, slowThread;
        struct sortingArray* smallArray;
        struct sortingArray* largeArray;
        int iCnt;
        int thFast, thSlow;	
	
	FILE* sortingResults;

	clock_t duration;

        srand(time(NULL));

	printf("Threaded QuickSort\npress enter to continue...\n");
	getchar();

	//sortingResults = fopen("sortingResults.csv", "a");
	for (iCnt = 0; iCnt < 25; iCnt++) {
	printf("loop: %i of %i\n", iCnt, 25);

        smallArray = (struct sortingArray*)malloc(sizeof(struct sortingArray));
        largeArray = (struct sortingArray*)malloc(sizeof(struct sortingArray));

	initSA(smallArray, iSize);
 	initSA(largeArray, iSize * iScaling);

        // Create thread handles

	duration = clock();

        thFast = pthread_create(&fastThread, NULL, quicksort, (void*)smallArray);
        thSlow = pthread_create(&slowThread, NULL, quicksort, (void*)largeArray);

        if (thFast != 0 || thSlow != 0) {
                printf("Threading error:\n %d \n %d\n", thFast, thSlow);
                //return -1;
		continue;
		continue;
        }

        pthread_join(fastThread, NULL);
        pthread_join(slowThread, NULL);

	duration = clock() - duration;

        printf("\n*********\n");
	displaySA(smallArray, 0);
   	displaySA(largeArray, 0);
	printf("Sorting:\nsmall: %i\nlarge: %i\nDuration: %d clicks  (%f seconds)\n", iSize, iSize * iScaling, duration, (float)duration / CLOCKS_PER_SEC);
	//fprintf(sortingResults, "%f,\n", (float)duration / CLOCKS_PER_SEC);
	
	free(smallArray->array);
	free(smallArray);

	free(largeArray->array);
	free(largeArray);

	} // loop
	
	//fclose(sortingResults);
	
       return 0;
}
//-----------------------------------------------------------------
void* quicksort(void* ptr)
{
        int leftCnt = 0;
        int rightCnt = 0;
        float* tmpLeft;
        float* tmpRight;
        int iPivotIdx, iIdx;
        float fPivot;
        struct sortingArray* leftArray;
	struct sortingArray* rightArray;

        struct sortingArray* array = (struct sortingArray*)ptr;
   
 	/*
        printf("quicksort:\n");
	for (iIdx = 0; iIdx < array->size; iIdx++) {
		printf("[%i] -> [%f]\n", iIdx, array->array[iIdx]);
 	}
 	*/

        if (array->size == 1) {
                return;
        }

	leftArray = (struct sortingArray*)malloc(sizeof(struct sortingArray));
	rightArray = (struct sortingArray*)malloc(sizeof(struct sortingArray));

        tmpLeft = (float*)calloc(array->size, sizeof(float));
        tmpRight = (float*)calloc(array->size, sizeof(float));

        iPivotIdx = array->size / 2;
        fPivot = array->array[iPivotIdx];

	//printf("\n\npivot: %f  pivotIdx: %i\n", fPivot, iPivotIdx);

        for (iIdx = 0; iIdx < array->size; iIdx++) {
                if (iIdx == iPivotIdx) {
                        continue;
                }

                if (array->array[iIdx] < fPivot) {
                        tmpLeft[leftCnt++] = array->array[iIdx];
                } else {
                        tmpRight[rightCnt++] = array->array[iIdx];
                }
        }

        array->size = 0;

	if (leftCnt > 0) {
        	leftArray->size = leftCnt;
        	leftArray->array = (float*)calloc(leftCnt, sizeof(float));
       		memcpy(leftArray->array, tmpLeft, leftCnt * sizeof(float));
              	free(tmpLeft);

        	//printf("left:\n");
        	//displaySA(leftArray, (leftCnt < 5));

            	quicksort((void*)leftArray);

        	memcpy(array->array + array->size, leftArray->array, leftArray->size * sizeof(float));
            	array->size += leftArray->size;

        	free(leftArray->array);
        	free(leftArray);
    	}

    	memcpy(array->array + array->size, &fPivot, 1 * sizeof(float));
        array->size++;

    	if (rightCnt > 0) {
           	rightArray->size = rightCnt;
 		rightArray->array = (float*)calloc(rightCnt, sizeof(float));
            	memcpy(rightArray->array, tmpRight, rightCnt * sizeof(float));
            	free(tmpRight);

            	//printf("right:\n");
        	//displaySA(rightArray, (rightCnt < 5));
   
            	quicksort((void*)rightArray);
       
        	memcpy(array->array + array->size, rightArray->array, rightArray->size * sizeof(float));
            	array->size += rightArray->size;
     
        	free(rightArray->array);
        	free(rightArray);
    	}
}
//---------------------------------------------------------------------------
void displaySA(struct sortingArray* array, int bShowValues)
{
    int iIdx;
   
    //printf("Array\n size: %i\n", array->size);
   
    if (bShowValues == 1) {
        for (iIdx = 0; iIdx < array->size; iIdx++) {
            printf("[%i] = [%f]\n", iIdx, array->array[iIdx]);
        }
    }
}
//---------------------------------------------------------------------------
void initSA(struct sortingArray* array, int iSize)
{
	//printf("Initializeing array\n");
    int iIdx;

    array->size = iSize;
        array->array = (float*)calloc(iSize, sizeof(float));

    for (iIdx = 0; iIdx < iSize; iIdx++) {
        array->array[iIdx] = (float)rand() / RAND_MAX;
    }
}
//---------------------------------------------------------------------------

